import json
import requests

original_titanic_scv = 'https://query.data.world/s/OuZzRk6mqxRar7_DS3PWH0v5Khy9Qw'
states_list = (
    'https://gist.githubusercontent.com/mshafrir/2646763/raw/8b0dbb93521f5d6889502305335104218454c2bf/states_hash.json'
)
states_info = (
    "https://raw.githubusercontent.com/python-visualization/folium/master/examples/data/us-states.json"
)
states_response = json.loads(requests.get(states_list).text)
states_list = list(states_response.keys())
