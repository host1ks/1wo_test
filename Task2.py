import folium
import numpy as np
import pandas as pd

from api.api import states_list, states_response, states_info, original_titanic_scv


class SurvivorsHeatmap:
    """
    Generate a heatmap for the USA with information about the number of survivors for each state, the source is the
    home_dest field from the original Dataset, countries other than USA and empty ones can simply be ignored, the
    colours are at your discretion, the states that are not present in the Dataset should be highlighted in a separate
    colour.
    Library of your choice (plotly, bokeh, folium, etc.)

    Implementation specifics:
    - the original file with the data should be obtained programmatically;
    - it is advisable to use list comprehensions where it is possible or optimal.
    """

    def __init__(self):
        # if I understood "the original file with the data should be obtained programmatically" correctly
        df = pd.read_csv(original_titanic_scv)
        self.cities = df['home.dest']
        self.f_map = folium.Map(location=[40, -95], zoom_start=4)
        super(SurvivorsHeatmap, self).__init__()

    def generate_heatmap(self):
        # generating dict of type { AL: (count), AK: (count) }
        for state in states_response:
            states_response[state] = 0

        for idx, city in self.cities.items():
            # skipping NaN
            if not isinstance(city, str):
                continue
            splitted_cities = city.split('/')
            for cities in splitted_cities:
                try:
                    # in case after splitting '/' remains the name of a city which not in USA
                    state_code = cities.split(',')[1].strip()
                    if state_code in states_list: states_response[state_code] += 1
                except IndexError:
                    pass
            # returns NaN instead of '0' which responds to folium
        for key in states_response:
            states_response[key] = np.nan if states_response[key] == 0 else states_response[key]

        folium.Choropleth(
            geo_data=states_info,
            name="choropleth",
            data=states_response,
            columns=["State", "Survived"],
            key_on="feature.id",
            fill_color="YlGn",
            line_opacity=.1,
            legend_name="Number of survivors (count)",
        ).add_to(self.f_map)

        folium.LayerControl().add_to(self.f_map)
        self.f_map.save("data/heatmap.html")


SurvivorsHeatmap().generate_heatmap()
