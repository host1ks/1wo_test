# README #
# To run tasks #
* source venv/bin/activate
* pip install -r requirements.txt
* ***Task1.py*** for Task 1 (result in data/new_titanic.txt)
* ***Task2.py*** for Task 2 (result in data/heatmap.html)

*In **result_example** directory you can find already generated results of tasks* 

### An additional question: ###
#### What factors can influence whether a passenger survived or not ? ####
In any case, a normal society will first of all give a chance to save children and women.
Undoubtedly, the disaster on the Titanic was no exception, 
it would not be foolish to miss the fact that one of the most important factors was prosperity. 
Therefore, this will be my answer. 
Unfortunately, to be able to save yourself and your family, 
you have to be in the first class.

#### What factors do not affect the situation at all? ####
In general, my opinions were not affected by such factors as the time of day, the presence of predators in the water, 
the design of the steamer itself.
If we evaluate the passengers themselves, I would say that the person’s lifestyle, sexual orientation, 
psychological habits did not make any impact.