from statistics import mean
import pandas as pd


class TitanicFamilies:
    """
    Using pandas, generate a new CSV containing a grouping by families, where the record is information about a
    single passenger or a family summary if family members are specified.
    Fields in the new Dataset:
    - type - 1/0 (one passenger or family);
    - name - last name (Allen, Allison);
    - survived - total number of survivors;
    - age - average age;
    - boats - list of boats separated by ','
    """

    def __init__(self):
        self.columns = ('type', 'name', 'survived', 'age', 'boats')
        self.df = pd.read_csv('data/titanic.csv')
        column_parch = self.df['parch']
        column_sibsp = self.df['sibsp']
        self.max_family = int(max(max(column_parch), max(column_sibsp)) + 1)

        self.new_titanic = pd.DataFrame(columns=self.columns)
        self.new_titanic.to_csv('data/new_titanic.csv', mode='a', index=False)

        self.last_surname = ''
        super(TitanicFamilies, self).__init__()

    def dict_generator(self, data):
        return dict(zip(self.columns, data))

    def generate_csv(self):
        for idx, row in self.df.iterrows():
            # looking for right data, because pandas gets row without data at the end of csv file
            full_name = row['name']
            # print(idx)
            if not isinstance(full_name, str):
                break
            else:
                surname, name = full_name.split(', ')

            # after I found a family via first member I want to skip another members of family and continue
            # looping from next surname
            if surname != self.last_surname:
                self.last_surname = surname
            else:
                continue
            # looking for members of family
            family = self.df[self.df.name.str.contains(f"{surname},", na=False)]
            # To avoid adding a family with the same surname
            # I look for people who are next to the current one in the list,
            # assuming that members of the family are in the same class
            family_range = range(idx - self.max_family, idx + self.max_family)

            # dropping namesakes
            new_family = family.copy()
            for idx_, person in new_family.iterrows():
                if idx_ not in family_range:
                    new_family.drop(idx_, inplace=True)

            if len(new_family) > 1:
                ages_average = mean(new_family['age'])
                boats = filter(lambda boat: isinstance(boat, str), new_family.boat.values)
                data = (
                    0,
                    surname,
                    int(new_family['survived'].sum()),
                    round(ages_average, 1),
                    [','.join(boats)]
                )
            else:
                # setting age as int, avoiding NaN
                age = new_family.age[idx]
                age = int(age) if age > 0 else age

                data = (
                    1,
                    new_family.name[idx],
                    int(new_family.survived[idx]),
                    age,
                    [new_family.boat[idx]]
                )
            new_titanic = pd.DataFrame(self.dict_generator(data))
            new_titanic.to_csv('data/new_titanic.csv', mode='a', header=False, index=False)


TitanicFamilies().generate_csv()
